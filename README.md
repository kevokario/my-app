# Angular Seed
This guide aims to quikly bootstrap an enterprise angular TDD app manually.

Project Setup
1. Development environment setup
2. Project Initialization
3. TDD Setup

    a. Jest for testing

    b. Linting

    c. Cypress e2e [Todo]

    d. Debugging

    e. Mocking
4. Deploy

    a. Firebase
  
    b. Gitlab CI-CD 
5. Troubleshooting

# Project Setup

Lets start by cloning this project as a starter template We'll create an angular app on top of it. 

```sh
# CLone and push to new project
git clone git@gitlab.com:zege/tools/ng-seed.git my-app
cd my-app
# Push to git repo
git remote set-url origin git@gitlab.com:your-account/your-repo.git
git push -u origin
```
Make sure to commit changes as you see fit through out the setup. 

## 1. Development environment setup
We develop with VS Code on Docker. In order to keep our local machine clean and avoid managing node and modules for projects centrally, we develop each project in a docker container. In order to integrate VS Code features into the docker container, we use the VS Code Remote container. This will allow us to user VS Code angular plugins and features like debugging. In summary we require the following
1. Docker and docker compose [installed](https://docs.docker.com/engine/installation/)
2. [VS Code IDE](https://code.visualstudio.com)
3. VS Code extension [remote containers](https://code.visualstudio.com/docs/remote/containers)

Our project contains a `.devcontainer` and `.vscode` folder that defines the above elements. 

Notice we are using an [ng-cli-e2e image](https://hub.docker.com/r/trion/ng-cli-e2e) as our dev environment. Be sure to check the most current image version and set the correct angular-cli version that corresponds to the angular version in `trion/ng-cli-e2e::$VERSION` [TODO: Use Node image instead]

Starting the dev container for the fisrt time will build the docker image as well as install plugins and setup IDE. You may also manually build the docker image by running `docker-compose build`

## 2. Project Initialization

### i. Setup Docker, docker-compose and VS Code dev container
Update your service and volume names in `.devcontainer/docker-compose.yml` and `.devcontainer/devcontainer.json`. You may need to authenticate with gitlab and add a private key. If already done so on local machine, then we mount that in the volumes for git to access repo. We also update out default git config message file in the compose file

### ii. Create your `node_modules` volume
```
docker volume create myapp-ng-node-modules
```

### iii. Launch VScode Dev Container
This will build your dev container and install vscode extensions

## 3. Initialize Angular Application
Before you do, delete the `README.md` otherwise generator will complain.
 
```sh
# Setup up autocompletion
source <(ng completion script)
# set yarn as package manager (optional)
ng config -g cli.packageManager yarn

# Create a new Angular CLI workspace
ng new <app-name> --directory .

# show default Angular app plaeeholder
ng serve --host 0.0.0.0 --disable-host-check
```

Update package.json script 
```json
...
"start": "ng serve --host 0.0.0.0 --disable-host-check",
```
## 4. TDD Setup

### i. Setup Jest

```sh
# Add packages
yarn add -D jest jest-preset-angular @types/jest
# Initilize jest.config.js file
yarn jest --init

```

Update `package.json` with the following
```json
  ...
    "test": "jest",
    "test:watch": "jest --watch",
    "test:ci": "jest --runInBand"
  ...
```

Update `./jest.config.js`

```
module.exports = {
  ...
  preset: "jest-preset-angular",
  setupFilesAfterEnv: ["<rootDir>/setup-jest.ts"],
  globalSetup: "jest-preset-angular/global-setup",
  ...
};

```

Add `./setup-jest.ts` file with the following
```ts
import 'jest-preset-angular/setup-jest';

```

### ii. Remove Karma and Jasmine
Delete `src/test.ts` and `./karma.conf.js`

Update `angular.json` by deleting `"test": {...}` 

Overwrite `tsconfig.spec.json` with the following

```json
/* To learn more about this file see: https://angular.io/config/tsconfig. */
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "outDir": "./out-tsc/spec",
    "module": "CommonJs",
    "types": ["jest"]
  },
  "include": ["src/**/*.spec.ts", "src/**/*.d.ts"]
}
```
Then run tests with 
```sh
yarn test

yarn test:watch
```



### iii. Linting
Install [eslint for angular](https://github.com/angular-eslint/angular-eslint). Be sure to match the current image version and set the correct angular-eslint version in `@angular-eslint/schematics$VERSION`


```sh
ng add @angular-eslint/schematics$VERSION
```

optionally add to lint eslint config
```json
"extends": [
    ...
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:@typescript-eslint/recommended"
  ],
```

Then run linting

```sh
# Run linter
ng lint
# auto correct lint errors
ng lint --fix
```

### iv. Debugging

#### 1. VS Code Debugger. 
Debugging is available through the VS code debugger launcher. Update the url from `localhost` to `127.0.0.1` then launch ng serve from vs code debug option. 
Binding should work. Only that sending commands from debugger to browser doesnt work. A manual refresh however reloads the launched browser. 

Debug test can be done thourgh the jest extension. Right click and select debug test

#### 2. [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) 
Redux DevTools for debugging application's state changes. It can be used with any other architectures which handle the state.

#### 3. [Angular Dev Tools](https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh)
Developer Tool extension for debugging and profiling Angular applications inside the Google Chrome browser.

### v. cypress e2e

https://github.com/cypress-io/cypress-documentation/issues/2956

e2e tests (UI tests) using [cypress](https://www.cypress.io/).

#### Install Cypress

```sh
yarn add -D cypress
```

#### Configure e2e testing(run in headed mode);use Electron Browser

```sh
yarn run cypress open
```

#### Then run tests in headless mode

```sh
yarn run cypress run
```

#### To add Cypress to test job in CI/CD pipeline

Install the wait-on module;ensure the dev server is set up before cypress tests are run

```sh
yarn add -D wait-on
```

`package.json`

```sh
{
  ...
  "scripts": {
    ...
    "cypress:run": "cypress run"
    "test:e2e": "yarn start & wait-on http://localhost:4200 && yarn run cypress:run" }
  ...
}
```

`gitlab-ci.yml`

```yml
...
test:
  image: trion/ng-cli-e2e:14.2.6
  stage: test
  script:
    - ng lint --fix
    - yarn test
    - yarn test:e2e
...
```

#### Now run tests with

```sh
yarn test:e2e
```

### v1. Mocking

Mocking via [ng-mocks](https://github.com/help-me-mom/ng-mocks#readme)
```
yarn add -D ng-mocks
```
## 5. Deploying to Firebase
### Setup Firebase
On terminal, move into your project folder locally, not in the dev container. 
We'll use the [docker-firebase image](https://hub.docker.com/r/andreysenov/firebase-tools) to get the `FIREBASE_TOKEN.

```sh
# Get Firebase token
docker run -i -t --rm -p 9005:9005 andreysenov/firebase-tools firebase login:ci

# Initialize firebase project in Angular app.
# Set public folder as `dist/my-app` or whatever is set in the `angular.json` `outputPath`
export FIREBASE_TOKEN=your-token
docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase init --token $FIREBASE_TOKEN

```

Add Firebase entries in `.gitignore`
```sh
# Google Services (ex. APIs and Firebase)
google-services.json
GoogleService-Info.plist

# Firebase Hosting
**/.firebase
firebase-debug.log
```

### Setup Firebase deploy targets and projects
Firebase recommends to create a separate project for different environments of your app for resource sharing concerns. In this case create a production project like 'myapp-ng' project and ideally a staging env project say 'company-staging-env' project to hold one or more staging env apps. 

Then from the UI, in the staging project, create you new `myapp-ng-stg` app by clicking add app. 

Grab the SITE_ID, which you will have created. You can also list and select SITE_IDs with 

```sh
docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase hosting:sites:list --token $FIREBASE_TOKEN
```

Add a deploy [target](https://firebase.google.com/docs/cli/targets) to `.firebaserc` for the staging app (`SITE_ID`) you choose by running command below

```sh
# Staging
export TARGET_NAME=staging
export PROJECT_NAME=stg-envs-proj #main project created for multisite staging deployments
export SITE_ID=myapp-ng-stg # created on firebase UI

docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase target:apply hosting $TARGET_NAME $SITE_ID --project $PROJECT_NAME --token $FIREBASE_TOKEN
```

Add a deploy [target](https://firebase.google.com/docs/cli/targets) to `.firebaserc` for the production app in a different isolated project by running command below

```sh
# Production
# Interactive prompt to select project and add PROJECT_ALIAS=production
docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase use --add --token $FIREBASE_TOKEN

export TARGET_NAME=production
export SITE_ID=myapp-ng
export PROJECT_NAME=myapp-ng

docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase target:apply hosting $TARGET_NAME $SITE_ID --project $PROJECT_NAME --token $FIREBASE_TOKEN

```

Update `firebase.json` configurations with the deploy targets
```json
...
"hosting": [
    {
      "target": "staging",
      "public": "dist/quikk-ng",
      ...
    },
    {
      "target": "production",
      "public": "dist/quikk-ng",
      ...
    }
  ]
```

Then run the following to deploy to available targets

```sh
# Run to deploy staging and production respectively

export TARGET_NAME=staging
export PROJECT_ALIAS=default # or production : default project is the staging project

docker run -i -t --rm \
  -v $PWD:/app \
  -w="/app" \
  andreysenov/firebase-tools firebase deploy --only hosting:$TARGET_NAME --project $PROJECT_NAME --token $FIREBASE_TOKEN
```

### Gitlab CI-CD
In the gitlab console, navigate to YOUR REPO > Settings > CI/CD > Variables.

Create a new variable `FIREBASE_TOKEN` in gitlab and assign it the value of the FIREBASE_TOKEN above as well as an environment. If you are using same account for both staging and production, there is not need to setting enviroment. 

Add a `./gitlab-ci.yaml` file to the project with the contents of `./gitlad-ci.example.yaml` . You can also just rename the file. Update the trion image with the current version.

Now, whenever you push to the master branch, tests will be run and deployment will happen to firebase staging project. Production deployment will only happen when you push to the release branch. 

Create a `release` branch and protect it in gitlab. This will be the branch that releases to production when pushed to. Merge master into release to deploy to production

### Gitlab Pages
Because your app will be built on pages using `--base-href=/ng-dev/myapp-ng/` make sure that your urls in scss files can be [resolved when deployed](https://stackoverflow.com/questions/42802431/angular-2-load-css-background-image-from-assets-folder) with or without `--base-href` by using relative prefix `~src/` as follows

```css
.elem{ 
  background-image: url(~src/assets/images/foo.png)
}
``` 

Gitlab pages will not identify your routes with SPA. https://gitlab.com/gitlab-org/gitlab-pages/-/issues/23

As a work around, you can deploy your app as a static Angular app using [Scully](https://scully.io). Setup the `pages` branch and initialize scully. 
  
`.gitlab-ci.pages.yml` is setup to deploy to pages when `pages` branch is pushed.

# Development

Development
1. Folder Structure
2. ngFrontend
2. ngBackend
3. Useful Tools

https://github.com/ngx-rocket/starter-kit/blob/main/docs/coding-guides/angular.md

## 1. Folder Structure
```
├── app
|   ├── session(module)
|   |   ├── session-component_n(login, confirmation)
|   |   ├── session-routing.module.ts
|   |   └── session.module.ts
|   |
|   ├── core
|   |   ├── [+] guards
|   |   ├── [+] http
|   |   ├── [+] interceptors
|   |   ├── [+] mocks
|   |   ├── [+] services
|   |   ├── core.module.ts
|   |   ├── ensureModuleLoadedOnceGuard.ts
|   |   └── logger.service.ts
|   |
|   ├── shared
|   |   ├── [+] components
|   |   ├── [+] directives
|   |   ├── [+] pipes
|   |   ├── [+] state
|   |   ├── [+] models
|   |   ├── [+] layouts
|   |   └── shared.module.ts
|   |
|   ├── pages(module)
|   |   ├── component_n
|   |   ├── pages-routing.module.ts
|   |   └── pages.module.ts
|   |
|   ├── home(module)
|       ├── component_n
|       ├── home-routing.module.ts
|       └── home.module.ts
|
├── styles
|   ├── theme
|   ├── _font-faces.scss
|   ├── _shared.scss
|   ├── _user-variables.scss
|   └── theme.scss
|
└── assets
    ├── fonts
    └── img
```

core module is for singletone and app wide imports, mostly those with `Plugin.forRoot()` notation and sitewide components and like navbar, header, footer etc. 

## 2. ngFrontend

1. Bootstrap Directives - [ng-bootstrap](https://classic.yarnpkg.com/en/package/@ng-bootstrap/ng-bootstrap)

2. Feather Icons - [angular-feather](https://feathericons.com/) and on [github](https://github.com/michaelbazos/angular-feather)

3. SVG icon utility - [angular-svg-icon](https://github.com/czeckd/angular-svg-icon)


## 3. ngBackend

<!-- TODO -->
### a. Backend Mocking
Backends need to be mocked for zero latency development. We use a fake backend that loads json responses taken from the backend data structure.

### b. State management
1. [ngxs](https://www.ngxs.io/)
Ensuring consistent state of an Angular app is important for non-trivial apps. 

# Troubleshooting
### Permissions
On linux you might run into permission issues while updating files. Fix by updating the file ownership on host machine.

```sh
# go into the angular project file directory
 cd ..
 sudo chown -R 1000:1000 ./
```

### Saas warnings

Use Saas migrator to fix 
https://sass-lang.com/documentation/breaking-changes/slash-div
```sh
npx sass-migrator division **/*.scss
```

### Jest types conflict with Cypress

Linting errors for Jest assertions e.g. property 'toBeTruthy' does not exist on type 'Assertion'.
Fix; exclude spec files in the base tsconfig.json and explicitly include them in the tsconfig.spec.json file that Jest uses:
`tsconfig.json`

```json
{
  ...
  "exclude": [
    "**/*.spec.ts"
  ]
  ...
}
```

`tsconfig.spec.json`

```json

{
  ...
  "include": ["src/**/*.spec.ts", "src/**/*.d.ts"]
  ...
}
```
